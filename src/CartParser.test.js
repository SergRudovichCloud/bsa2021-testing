import CartParser from './CartParser';

let parser, calcTotal, validate, parseLine, parse;

beforeEach(() => {
	parser = new CartParser();
	calcTotal = parser.calcTotal;
	validate = parser.validate.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	parse = parser.parse.bind(parser);
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	it('should return total sum of Cart ', () => {
		const items = [
			{
				price: 10,
				quantity: 2
			},
			{
				price: 5,
				quantity: 2
			}
		]
		expect(calcTotal(items)).toEqual(30);
	});

	it('should return error wrong header name Cost instead Price', () => {
		const contents = `Product name,Cost,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1`;
		expect(validate(contents))
			.toEqual([{ "column": 1, "message": "Expected header to be named \"Price\" but received Cost.", "row": 0, "type": "header" }]);
	});

	it('should return error expected row to have 3 cells but received 2', () => {
		const contents = `Product name,Price,Quantity
		Mollis consequat,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1`;
		expect(validate(contents))
			.toEqual([{ "column": -1, "message": "Expected row to have 3 cells but received 2.", "row": 1, "type": "row" }]);
	});

	it('should return error Expected cell to be a nonempty string but received \"\"', () => {
		const contents = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		,10.32,1
		Scelerisque lacinia,18.90,1`;
		expect(validate(contents))
			.toEqual([{ "column": 0, "message": "Expected cell to be a nonempty string but received \"\".", "row": 2, "type": "cell" }]);
	});

	it('should return error Expected cell to be a positive number but received \"olala\"', () => {
		const contents = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,olala`;
		expect(validate(contents))
			.toEqual([{ "column": 2, "message": "Expected cell to be a positive number but received \"olala\".", "row": 3, "type": "cell" }]);
	});

	it('should return error Expected cell to be a positive number but received \"-1\"', () => {
		const contents = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,-1
		Scelerisque lacinia,18.90,1`;
		expect(validate(contents))
			.toEqual([{ "column": 2, "message": "Expected cell to be a positive number but received \"-1\".", "row": 2, "type": "cell" }]);
	});

	it('should return array of three error message objects', () => {
		const contents = `Product name,Cost,Quantity
		,9.00,2
		Tvoluptatem,10.32,-1
		Scelerisque lacinia,18.90,1`;
		expect(validate(contents)).toHaveLength(3);
	});

	it('should return empty array of errors', () => {
		const contents = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1`;
		expect(validate(contents)).toEqual([]);
	});

	it('should return parsed line', () => {
		const csvLine = `Mollis consequat,9.00,2`;
		expect(parseLine(csvLine)).toHaveProperty("name", "Mollis consequat");
		expect(parseLine(csvLine)).toHaveProperty("price", 9);
		expect(parseLine(csvLine)).toHaveProperty("id", expect.any(String));
	});
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	it('should be call parseLine 5 times', () => {
		parser.readFile = jest.fn(() => {return (`Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`)});

		parser.parseLine = jest.fn(()=>{return {"id": "a3703cb0-2480-4dc4-8b0c-33abcf426bb9", "name": "Mollis consequat", "price": 9, "quantity": 
		2}});

		parser.parse('');

		expect(parser.parseLine).toHaveBeenCalledTimes(5);
	});

});